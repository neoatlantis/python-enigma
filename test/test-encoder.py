from os import urandom
from enigma.identity import PrivateIdentity, PublicIdentity
from enigma.encoder import encode, decode

##############################################################################

print "Generate a private identity."
prv1 = PrivateIdentity()
prv1.generate('test')

pub1 = prv1.getPublicIdentity()

print encode(prv1)

print encode(pub1)
