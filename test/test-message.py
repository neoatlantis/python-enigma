from os import urandom

from enigma.message import MessagePayload, MessageCryptor
from enigma.identity import PrivateIdentity
from enigma.payload import PlaintextPayload

testinfo = PlaintextPayload()
testinfo.fill(urandom(100))

print "Test composing a new message."
msg1 = MessagePayload().message(testinfo)

print "Generation of several identities."
prv1 = PrivateIdentity()
prv2 = PrivateIdentity()
prv3 = PrivateIdentity()
prv1.generate("Alice")
prv2.generate("Bob")
prv3.generate("Carol")
pub1 = prv1.getPublicIdentity()
pub2 = prv2.getPublicIdentity()
pub3 = prv3.getPublicIdentity()

print "Test sign the message."
sig1 = msg1.sign(prv1)
print '--> message signed by', msg1.listSignatures()

print "Test serialization of message payload."
msg1s = str(msg1)
print repr(msg1)
msg2 = MessagePayload(msg1s)
assert msg2.message() == testinfo

print "Test verifying the signature in payload."
assert msg2.verify(pub1)

print "Test manipulation on a deserialized message payload."
msgx = MessagePayload(msg1s)
msgxp = msgx.message()
msgxp.plaintext = 'manipulated'
msgx.message(msgxp)
print '--> Manipulated message still has following signatures remembered',
print msgx.listSignatures()
assert not msgx.verify(pub1)

print "Test encryption of message payload."
enc1 = MessageCryptor().payload(msg2)
print repr(enc1)

enc1.encrypt(pub1)
enc1.encrypt(pub2)
enc1.encrypt(pub3, True)

cpt1 = str(enc1).encode('base64')
print repr(enc1) 

print "Test decryption of encrypted message payload."
cpt2 = cpt1.decode('base64')
dec1 = MessageCryptor(cpt2)
print repr(dec1)

print '--> message encrypted to', dec1.listReceivers()

decmsg1 = dec1.decrypt(prv2)
assert decmsg1.message() == testinfo
assert decmsg1.verify(pub1)
print repr(dec1)

print "Test decryption against anonymous hint."

dec2 = MessageCryptor(cpt2)
decmsg2 = dec2.decrypt(prv3, True)
assert decmsg2.message() == testinfo
