from os import urandom
from enigma.identity import PrivateIdentity, PublicIdentity

##############################################################################

msg1 = urandom(100)
msg2 = urandom(100)

pwd1 = urandom(32)
pwd2 = urandom(32)

##############################################################################

print "Generate a private identity."
prv1 = PrivateIdentity()
prv1.generate('Test Identity with an enough long subject area.', pwd1)
print repr(prv1)
print str(prv1).encode('base64')

print "Load the generated private identity again, test serialization."
prv2 = PrivateIdentity(str(prv1), pwd1)
print repr(prv2)

print "Test changing private identity's passphrase."
prv3 = PrivateIdentity(str(prv1), pwd1)
prv3.passphrase = pwd2
prv3 = PrivateIdentity(str(prv3), pwd2)
print repr(prv3)

print "Test a not fully loaded private identity."
prvx = PrivateIdentity(str(prv1))
print repr(prvx)

print "Test signature and validation."
sig1 = prv1.sign(msg1)

print "Generate public key part of private identity."
pub1 = prv1.getPublicIdentity()
print repr(pub1)
print str(pub1).encode('base64')

print "Load public key part again, test serialization."
pub2 = PublicIdentity(str(pub1))
print repr(pub2)

##############################################################################

print "Verify the signature using public key."
assert True == pub2.verify(msg1, sig1)
assert False == pub2.verify(msg2, sig1)

##############################################################################

print "Test encryption using public key."
enc2 = pub2.encrypt(msg1)

print "Test decryption using private key."
dec1 = prv1.decrypt(enc2)
assert msg1 == dec1
