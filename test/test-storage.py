from enigma.storage import Storage
from enigma.identity import PrivateIdentity, PublicIdentity

passphrase = 'this a long enough passphrase'

print "Generate a private identity."
prv1 = PrivateIdentity()
prv1.generate('test')

with Storage() as storage:

    print "List saved private identities."
    storedPrivate = storage.listPrivateIdentities()
    print storedPrivate

    print "List saved public identities."
    storedPublic = storage.listPublicIdentities()
    print storedPublic

    if len(storedPrivate) < 1:
        storage.privateIdentity(prv1, passphrase)
    
    print "Decrypt each private identity."
    for i in storedPrivate:
        print "Decrypt key [%s]." % i
        prvi = storage.privateIdentity(i, passphrase)
