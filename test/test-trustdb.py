import time

from enigma.storage import Storage
from enigma.identity import PrivateIdentity, PublicIdentity
from enigma.message import MessagePayload
from enigma.payload import IdentityTrustPayload


passphrase = 'this a long enough passphrase'

with Storage() as storage:

    print "List saved private identities."
    storedPrivate = storage.listPrivateIdentities()
    print storedPrivate

    print "List saved public identities."
    storedPublic = storage.listPublicIdentities()
    print storedPublic

    fp = storedPrivate.keys()[0]
    print "Read identity %s" % fp
    prv = storage.privateIdentity(fp, passphrase)
    pub = storage.publicIdentity(fp)

    print "List TrustDB entries of PublicIdentity %s." % fp
    trustdb = storage.getTrustDB(pub)

    trusts = trustdb.reportTrusts()
    print trusts
    if len(trusts) <= 3:
        print "Will generate some trust signatures to fill the trustdb."

        trust = IdentityTrustPayload()
        trust.fill(\
            pub.getFingerprint(), # destinated
            {"test": "a", "test2": "b", "test3": str(time.time())},
            None,
            None
        )

        msg = MessagePayload()
        msg.message(trust)
        msg.sign(prv)

        trustdb.attachPayload(msg)

    else:
        print "Test verified trusts."
        vtrusts = trustdb.reportValidTrusts()

        print vtrusts
