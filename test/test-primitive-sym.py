from os import urandom
from enigma.permitive.sym import Encryptor, Decryptor

enc = Encryptor()
msg = urandom(100) 
cpt = enc.encrypt(msg)
key = enc.getKey()

dec = Decryptor(key)
assert dec.decrypt(cpt) == msg
