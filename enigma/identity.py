import os
import base64
import re

from .primitive.asym import EncryptKey, DecryptKey, SignKey, VerifyKey 
from .primitive.sym import Encryptor, Decryptor
from .primitive.random import randomBytes
from .primitive.hash import hash256, hash512, hmac256, hmac256, pbkdf2
from .serialize.elements import ShortBuffer, LargeBuffer, ConstantBuffer
from .serialize.structure import Structure

from .exceptions import IdentityUnserializationError,\
                        IdentityInvalidSubject,\
                        IdentityInvalidSelfSignature,\
                        IdentityInvalidPassphrase,\
                        IdentityNotUsable

##############################################################################

# -------- Define base identity class 

class _BaseIdentity:

    def __filterSubject(self, subject):
        if type(subject) != str:
            raise IdentityInvalidSubject
        if None == re.match('^[\x20-\x7E]{3,128}$', subject):
            raise IdentityInvalidSubject
        return subject

# -------- Define the PublicIdentity class

class PublicIdentity(_BaseIdentity):

    __structure = Structure(\
        _=ConstantBuffer('I'),
        encryptKey=ShortBuffer(),
        verifyKey=ShortBuffer(),
        subject=ShortBuffer(),
        selfSignature=ShortBuffer()
    )

    subject=None
    
    def __loadPublicIdentity(self, s):
        """Load a public identity from a given `s`(string), and do validation
        with its self-signature."""
        parsed = self.__structure.loads(s)
        if parsed == None:
            raise IdentityUnserializationError
        # load parameters
        self.subject = self._BaseIdentity__filterSubject(parsed.subject)
        self.encryptKey = parsed.encryptKey
        self.verifyKey = parsed.verifyKey
        self.selfSignature = parsed.selfSignature
        # update identity
        self.__updateIdentity()

    def __updateIdentity(self):
        self.__encryptKey = EncryptKey(self.encryptKey)
        self.__verifyKey = VerifyKey(self.verifyKey)
        # load self signature
        selfSig = self.selfSignature
        fingerprint = self.getFingerprint()
        if not self.__verifyKey.verify(fingerprint, selfSig):
            raise IdentityInvalidSelfSignature
    
    def __init__(self, s=None):
        if s != None:
            self.__loadPublicIdentity(s)

    def __str__(self):
        return self.__structure.dumps(\
            _='I',
            encryptKey=self.encryptKey,
            verifyKey=self.verifyKey,
            subject=self.subject,
            selfSignature=self.selfSignature
        )

    def __repr__(self):
        if self.subject == None: return "<PublicIdentity: empty>"
        ret = "<PublicIdentity\n"
        ret += "  FingerPrint: %s\n" % self.getFingerprint()
        ret += "  Subject: %s\n" % self.subject
        ret += ">"
        return ret

    def getFingerprint(self):
        """Calculates a fingerprint based on `self.subject`, `self.encryptKey`
        and `self.verifyKey`. This identifies the public identity, and the
        string is also used internally to sign this identity."""
        h1 = hash512(self.subject)
        h2 = hash512(self.encryptKey)
        h3 = hash512(self.verifyKey)
        h = hash256(h1 + h2 + h3)
        return base64.b32encode(h)[:16].lower()

    def verify(self, message, signature):
        return self.__verifyKey.verify(message, signature)

    def encrypt(self, plaintext):
        return self.__encryptKey.encrypt(plaintext)

# -------- Define the PrivateIdentity class
#
#          A PrivateIdentity object has following attributes to be recorded:
#           1. encryptedSecret
#           2. fingerprint
#           3. subject
#           4. checksum
#          This object can be loaded only with a given valid passphrase. This
#          passphrase decrypts the `encryptedSecret` and works as an
#          examination of `fingerprint`, `subject` by HMACing their hashes
#          hashes. This prevents a modification without knowledge of the
#          passphrase.
#          Passphrase for PrivateIdentity is necessarily a connection of the
#          stored secret data and the user's physical identity.
#
#          A PrivateIdentity instance can be serialized using str(), and
#          deserialized with or without presenting a passphrase. With
#          passphrase given, the instance will be fully usable, e.g. can be
#          used for deriving a self-signed public key, signing or decrypting.
#          If not, only the `subject` and `fingerprint` will be loaded without
#          a throughly verification. The latter case can be used for fastly
#          examining a key and get its metadata.

PASSPHRASE_MIN_LENGTH = 10
PASSPHRASE_DERIVATION_ITERATION = 65536

class PrivateIdentity(_BaseIdentity):

    __structure = Structure(\
        _=ConstantBuffer('i'),
        encryptedSecret=ShortBuffer(),
        fingerprint=ShortBuffer(),
        subject=ShortBuffer(),
        checksum=ShortBuffer(),
    )

    subject = None

    ## ---- Regarding management, encryption and decryption of identity
    ##      secrets.

    __secret = None
    __encryptedSecret = None
    __passphraseForEncryptedSecret = None
    passphrase = None

    def __getSecretStorageKey(self, passphrase):
        if type(passphrase) != str:
            raise IdentityInvalidPassphrase
        if len(passphrase) < PASSPHRASE_MIN_LENGTH:
            raise IdentityInvalidPassphrase('too-short', PASSPHRASE_MIN_LENGTH)
        # The following `self.fingerprint` is taken into account even when
        # the real secret is not decrypted. In such case, a pre-recorded
        # value is taken use. This has no harm since after successful
        # decryption we need to verify that.
        return pbkdf2(\
            passphrase,
            self.subject,
            PASSPHRASE_DERIVATION_ITERATION,
            32
        )
    
    def __loadEncryptedSecret(self, encryptedSecret, passphrase):
        """Load a given encryptedSecret into identity. It has to be decrypted
        to be used."""
        # try to decrypt
        decryptKey = self.__getSecretStorageKey(passphrase)
        decryptor = Decryptor(decryptKey)
        secret = decryptor.decrypt(encryptedSecret)
        if None == secret: raise IdentityInvalidPassphrase()
        # load secret if successfully decrypted
        self.__secret = secret
        self.__encryptedSecret = encryptedSecret
        self.passphrase = passphrase
        self.__passphraseForEncryptedSecret = passphrase

    def __generateEncryptedSecret(self, newSecret=None, newPassphrase=None):
        """Generate the `encryptedSecret` basing on `self.passphrase` and
        `self.__secret`."""
        if None == newSecret:
            secret = self.__secret
            passphrase = self.passphrase
        else:
            secret = newSecret
            passphrase = newPassphrase
        # if (secret, passphrase) are not modified, just return the cached
        # value
        if \
            secret == self.__secret and\
            passphrase == self.__passphraseForEncryptedSecret:
            return self.__encryptedSecret # cached
        # encrypt the secret, and update cache
        encryptKey = self.__getSecretStorageKey(passphrase)
        encryptor = Encryptor(encryptKey)
        encryptedSecret = encryptor.encrypt(secret)
        self.__secret = secret
        self.__encryptedSecret = encryptedSecret
        self.passphrase = passphrase
        self.__passphraseForEncryptedSecret = passphrase

    ## ---- Checksum calculation of a loaded identity

    def __calculateChecksum(self):
        """Calculate a checksum using `self.passphrase`."""
        h1 = hash512(self.subject)
        h2 = hash512(self.fingerprint)
        h3 = hash512(self.__encryptedSecret)
        h = hmac256(h1 + h2 + h3, self.passphrase)
        return h 

    ## ---- Regarding loading the identity from a serialized string.
    
    def __loadPrivateIdentity(self, s, passphrase):
        # clearance
        self.subject, self.fingerprint = None, None
        self.__secret = None
        self.__signKey, self.__decryptKey = None, None
        
        # Deserialization of `s`.
        parsed = self.__structure.loads(s)
        if parsed == None: raise IdentityUnserializationError
        # Load `subject` and `fingerprint`
        self.subject = self._BaseIdentity__filterSubject(parsed.subject)
        self.fingerprint = parsed.fingerprint

        # If passphrase is not given, only above deserialization is done,
        # for purposes like examining a key roughly.
        if passphrase == None: return

        # Decrypt `encryptedSecret`
        self.__loadEncryptedSecret(parsed.encryptedSecret, passphrase)
        # If that's ok, self.__secret and others should be set.
        self.__updateIdentity()
        # Verify checksum
        if not self.__calculateChecksum() == parsed.checksum:
            raise IdentityUnserializationError('checksum-error')
        # Derive fingerprint, ensure that recorded is the one revealed after
        # decryption.
        if not parsed.fingerprint == self.fingerprint:
            raise IdentityUnserializationError()

    def __updateIdentity(self):
        """Derive secrets used for signing and encrypting with self.secret"""
        sSecret = hmac256(self.__secret, 'sign')
        eSecret = hmac256(self.__secret, 'encrypt')
        self.__signKey = SignKey(sSecret)
        self.__decryptKey = DecryptKey(eSecret)
        self.fingerprint = self.getFingerprint()

    ## ----
    
    def __init__(self, s=None, passphrase=None):
        """Load from a PrivateIdentity if `s` is not None. In this case
        `passphrase` can be given to fully load the identity and make use of
        its signing or decrypting capabilities. Otherwise there will be no
        `secret` decrypted, and this instance may only be used as an
        examination on this PrivateIdentity for its `subject` and
        `fingerprint`."""
        if None != s: self.__loadPrivateIdentity(s, passphrase)

    def decrypt(self, ciphertext):
        if not self.__secret: raise IdentityNotUsable()
        return self.__decryptKey.decrypt(ciphertext)

    def sign(self, text):
        if not self.__secret: raise IdentityNotUsable()
        return self.__signKey.sign(text)

    def generate(self, subject, passphrase):
        """Generates a new private identity."""
        secret = randomBytes(64)
        self.subject = self._BaseIdentity__filterSubject(subject)
        self.__generateEncryptedSecret(secret, passphrase)
        self.__updateIdentity()

    def __str__(self):
        if not self.__secret: raise IdentityNotUsable()
        self.__generateEncryptedSecret() # ensure newest encryptedSecret
        checksum = self.__calculateChecksum()
        return self.__structure.dumps(\
            _='i',
            encryptedSecret=self.__encryptedSecret,
            subject=self.subject,
            checksum=checksum,
            fingerprint=self.fingerprint
        )

    def __repr__(self):
        if self.subject == None: return "<PrivateIdentity: empty>"
        usable = (self.__secret != None)
        fp = self.fingerprint
        ret = "<PrivateIdentity\n"
        ret += "  FingerPrint: %s\n" % fp 
        ret += "  Subject: %s\n" % self.subject
        if not usable: ret += "  Not usable. Not fully verified.\n"
        ret += ">"
        return ret

    def getPublicIdentity(self):
        if not self.__secret: raise IdentityNotUsable()
        ret = PublicIdentity()
        # first set basic parameters
        ret.subject = self.subject
        ret.encryptKey = str(self.__decryptKey.getEncryptKey())
        ret.verifyKey = str(self.__signKey.getVerifyKey())
        # then generate a self signature
        fingerprint = ret.getFingerprint()
        selfSig = self.__signKey.sign(fingerprint)
        # attach this self signature
        ret.selfSignature = selfSig
        ret._PublicIdentity__updateIdentity()
        return ret

    def getFingerprint(self):
        return self.getPublicIdentity().getFingerprint()
