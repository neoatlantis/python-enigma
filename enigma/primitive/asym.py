import nacl.public
import nacl.signing
from nacl.encoding import RawEncoder

from .random import randomBytes
from ..serialize.elements import ShortBuffer, LargeBuffer, ConstantBuffer
from ..serialize.structure import Structure

# -------- Asymmetric Algorithms for Encrypting/Decrypting 

ciphertextStructure = Structure(\
    _=ConstantBuffer('c'),
    randPublicKey=ShortBuffer(),
    payload=LargeBuffer()
)

class EncryptKey:
    
    def __init__(self, public):
        self.__encryptKey = nacl.public.PublicKey(public)

    def __str__(self):
        return str(self.__encryptKey)

    def encrypt(self, plaintext):
        # First generate a randKey. We're not using PrivateKey.generate() but
        # our own randomBytes() function.
        randSecret = randomBytes(nacl.public.PrivateKey.SIZE)
        randKey = nacl.public.PrivateKey(randSecret, RawEncoder)
        box = nacl.public.Box(randKey, self.__encryptKey)
        nonce = randomBytes(nacl.public.Box.NONCE_SIZE)
        encrypted = box.encrypt(plaintext, nonce)
        ret = ciphertextStructure.dumps(\
            _='c',
            randPublicKey=str(randKey.public_key),
            payload=str(encrypted)
        )
        return ret

class DecryptKey:

    def __init__(self, secret):
        self.__decryptKey = nacl.public.PrivateKey(secret, RawEncoder)

    def __str__(self):
        return str(self.__decryptKey)

    def decrypt(self, ciphertext):
        parsed = ciphertextStructure.loads(ciphertext)
        if None == parsed: return None
        
        randPublicKey = parsed.randPublicKey
        ciphertext = parsed.payload

        box = nacl.public.Box(\
            self.__decryptKey,
            nacl.public.PublicKey(randPublicKey, RawEncoder)
        )
        try:
            plaintext = box.decrypt(ciphertext)
        except:
            return None
        return plaintext 

    def getEncryptKey(self):
        return EncryptKey(str(self.__decryptKey.public_key))

# -------- Asymmetric Algorithms for Signing/Verifying

class VerifyKey:
    
    def __init__(self, public):
        self.__verifyKey = nacl.signing.VerifyKey(public, RawEncoder)

    def __str__(self):
        return str(self.__verifyKey)

    def verify(self, message, signature):
        try:
            self.__verifyKey.verify(message, signature, RawEncoder)
            return True
        except Exception,e:
            return False

class SignKey:

    def __init__(self, secret):
        self.__signKey = nacl.signing.SigningKey(secret, RawEncoder)

    def __str__(self):
        return str(self.__signKey)

    def sign(self, message):
        sig = self.__signKey.sign(message).signature
        return sig

    def getVerifyKey(self):
        return VerifyKey(str(self.__signKey.verify_key))
