import hashlib
import hmac
from pbkdf2 import pbkdf2

def hash160(src): return hashlib.sha1(src).digest()
def hash256(src): return hashlib.sha256(src).digest()
def hash512(src): return hashlib.sha512(src).digest()

def hmac160(src, key): return hmac.HMAC(key, src, hashlib.sha1).digest()
def hmac256(src, key): return hmac.HMAC(key, src, hashlib.sha256).digest()
def hmac512(src, key): return hmac.HMAC(key, src, hashlib.sha512).digest()

