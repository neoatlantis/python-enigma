import nacl.secret
import nacl.utils

from .random import randomBytes 


class Encryptor:

    def __init__(self, key):
        self.__box = nacl.secret.SecretBox(key)

    def encrypt(self, plaintext):
        nonce = randomBytes(nacl.secret.SecretBox.NONCE_SIZE)
        return str(self.__box.encrypt(plaintext, nonce))

class Decryptor:
    
    def __init__(self, key):
        self.__box = nacl.secret.SecretBox(key)

    def decrypt(self, ciphertext):
        try:
            return self.__box.decrypt(ciphertext)
        except:
            return None
