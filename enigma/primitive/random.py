import time
import math
import os
import hashlib
import nacl.utils

def _KDF1(secret, length):
    hashfunc, hlen = hashlib.sha512, 64
    d = int(math.ceil(length * 1.0 / hlen))
    T = ''
    for i in xrange(0, d):
        C = str(i)
        T += hashfunc(secret + C).digest()
    return T[:length]

def randomBytes(count):
    """Random bytes will be generated using XOR of 3 parallel random sources.
    Each source produces same length of output bytes as the final output should
    require. Among `nacl.util.random` and `os.urandom` these 2 traditional
    cryptographically secure sources, another source, which is based on
    sampling the CPU speed for 8 * count times and the SHA512 algorithm is
    added. For `count` bytes, `8 * count` times should guarantee that even only
    the differences between elasped times of samplings, whether the first is
    larger than the second, which provides at least 1 bit, could provide enough
    entropy for final requirement. And we know from the One-Time-Pad encryption
    theory that as long as one source in our final mix-ups is secure, then the
    others, even if they are predictable, will not matter."""
    timesrc = ''.join([str(time.time()) for i in xrange(0, count * 8)])
    timesrc = _KDF1(timesrc, count)

    srcs = []
    srcs.append(bytearray(timesrc))
    srcs.append(bytearray(nacl.utils.random(count)))
    srcs.append(bytearray(os.urandom(count)))

    output = srcs[0]
    srclen = len(srcs)
    for i in xrange(1, srclen):
        for j in xrange(0, count):
            output[j] ^= srcs[i][j]
    
    return str(output)
