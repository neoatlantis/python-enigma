"""
Provides local storage on a system, defaults to ~/.enigma.db
"""
import anydbm
import os
from json import dumps, loads

from .identity import PublicIdentity, PrivateIdentity
from .exceptions import IdentityUnserializationError, \
                        IdentityInvalidSubject,\
                        IdentityInvalidSelfSignature,\
                        IdentityInvalidPassphrase,\
                        IdentityNotUsable
from .exceptions import StorageOpenedReadOnly,\
                        StorageIdentityNotFound,\
                        StorageIdentityInvalid
                    
from .trustdb import TrustDB

PASSPHRASE_MIN_LENGTH = 10
PASSPHRASE_DERIVATION_ITERATION = 65536

class Storage:

    def __init__(self, **argv):
        if argv.has_key('path'):
            self.__path = argv['path']
        else:
            self.__path = os.path.join(
                os.path.realpath(os.path.expanduser('~')),
                '.enigma.db'
            )
        if argv.has_key('readOnly'):
            self.__ro = bool(argv['readOnly'])
        else:
            self.__ro = False

    def __getPrivateIdentityDBKey(self, fp):
        return 'private-%s' % fp

    def __getPublicIdentityDBKey(self, fp):
        return 'public-%s' % fp

    def listPrivateIdentities(self):
        fpList = [i[8:] for i in self.__db if i.startswith('private-')]
        ret = {}
        for fingerprint in fpList:
            dbKey = self.__getPrivateIdentityDBKey(fingerprint)
            dbItem = loads(self.__db[dbKey])
            ret[fingerprint] = {
                'subject': dbItem["subject"],
                'type': 'private',
                'fingerprint': fingerprint,
            }
        return ret

    def listPublicIdentities(self):
        fpList = [i[7:] for i in self.__db if i.startswith('public-')]
        ret = {}
        for fingerprint in fpList:
            dbKey = self.__getPublicIdentityDBKey(fingerprint)
            dbItem = loads(self.__db[dbKey])
            ret[fingerprint] = {
                'subject': dbItem["subject"],
                'type': 'public',
                'fingerprint': fingerprint,
            }
        return ret 

    def publicIdentity(self, identity):
        """Set or retrieve a PublicIdentity instance. If `identity` is a
        string, it will be interpreted as a fingerprint for searching.
        Otherwise it is assumed to be an instance of PublicIdentity."""

        if type(identity) == str:
            # Get a public identity from storage
            # ---- first find the identity
            dbKey = self.__getPublicIdentityDBKey(identity)
            if not self.__db.has_key(dbKey):
                raise StorageIdentityNotFound
            dbItem = loads(self.__db[dbKey])
            # ---- then try to resume the instance of identity, this may
            #      raise exception. Whatever an exception is raised, this
            #      entry has to be considered as wrong, and will be deleted.
            #      The examination on the fingerprint is also done. If what
            #      we have retrieved doesn't has a fingerprint marked on
            #      the index, it will be deleted.
            loadError = False
            try:
                identityPublic = PublicIdentity(\
                    dbItem["data"].decode('base64')
                )
                if identityPublic.getFingerprint() != identity:
                    loadError = True
            except (\
                IdentityUnserializationError,\
                IdentityInvalidSubject,\
                IdentityInvalidSelfSignature
            ):
                loadError = True
            finally:
                if loadError:
                    if not self.__ro: del self.__db[dbKey]
                    raise StorageIdentityNotFound
            # ---- if nothing happened, we will return this identity.
            return identityPublic

        if isinstance(identity, PublicIdentity):
            if self.__ro: raise StorageOpenedReadOnly
            identityFingerprint = identity.getFingerprint()
            dbKey = self.__getPublicIdentityDBKey(identityFingerprint)
            self.__db[dbKey] = dumps({
                "data": str(identity).encode('base64'),
                "subject": identity.subject,
            })
            return

        raise StorageIdentityNotFound

    def privateIdentity(self, identity):
        """Set or retrieve a PrivateIdentity.

        If `identity` is a string, it will be interpreted as a fingerprint.
        Otherwise it is assumed an instance of PrivateIdentity. 
        **NOTICE: The PrivateIdentity to be imported has to be usable.**
        By saving a private identity, its corresponding public identity will
        also be registered.
        """
        if type(identity) == str:
            # Retrieve an identity with given fingerprint
            # ---- first find out the entry in database
            identityFingerprint = identity
            dbKey = self.__getPrivateIdentityDBKey(identityFingerprint)
            if not self.__db.has_key(dbKey): raise StorageIdentityNotFound()
            # ---- generate the instance of PrivateIdentity for return value.
            #      This may cause exception. When it happens, we'll consider
            #      this entry as bad and pretend to have found nothing.
            try:
                dbItem = loads(self.__db[dbKey])
                identityPrivate = PrivateIdentity(\
                    dbItem["data"].decode('base64')
                )
            except:
                # in contrast to public identity exception handling, we'll not
                # delete the entry.
                raise StorageIdentityNotFound
            # ---- check if the exported public identity does have a
            #      fingerprint as searched. If not, this entry will be deleted.
            if identityPrivate.fingerprint != identityFingerprint:
                if not self.__ro: del self.__db[dbKey]
                raise StorageIdentityNotFound
            # ---- return value
            return identityPrivate

        if isinstance(identity, PrivateIdentity):
            if self.__ro: raise StorageOpenedReadOnly
            # Save the given identity and its public identity using given
            # passphrase
            # ---- first derive the public part
            try:
                identityPublic = identity.getPublicIdentity()
                identityFingerprint = identityPublic.getFingerprint()
            except:
                raise StorageIdentityInvalid 
            # ---- then save the private encrypted
            self.__db[dbKey] = dumps({
                "data": str(identity).encode('base64'),
                "subject": identity.subject,
            })
            # ---- save public separately
            self.publicIdentity(identityPublic)
            return

        raise StorageIdentityNotFound

    def getTrustDB(self, publicIdentity):
        if not isinstance(publicIdentity, PublicIdentity):
            raise StorageIdentityNotFound
        return TrustDB(self, publicIdentity.getFingerprint())

    def __enter__(self):
        if self.__ro:
            try:
                self.__db = anydbm.open(self.__path, 'r')
            except:
                raise StorageOpenedReadOnly
        else:
            self.__db = anydbm.open(self.__path, 'c')
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.__db.close()
