from .translation import translate

# define exceptions that can be emitted

class _BaseEnigmaException(Exception):
    def __str__(self):
        args = self.args
        name = type(self).__name__
        if len(args) < 1: return name
        errname, errargs = args[0], args[1:]
        if not (hasattr(self, 't') and self.t.has_key(errname)):
            raise Exception("Exception [%s] not translated yet." % errname)
        translateRule = self.t[errname]
        translateName = translateRule[0]
        translateArgs = [errargs[i] for i in list(translateRule[1:])]
        return translate(translateName, translateArgs)


class MessageInvalidSigner(_BaseEnigmaException): pass
class MessageInvalidReceiver(_BaseEnigmaException): pass
class MessageInvalidPayload(_BaseEnigmaException): pass
class MessageInvalidCiphertext(_BaseEnigmaException): pass

class StorageOpenedReadOnly(_BaseEnigmaException): pass
class StorageIdentityNotFound(_BaseEnigmaException): pass
class StorageIdentityInvalid(_BaseEnigmaException): pass

class IdentityUnserializationError(_BaseEnigmaException): pass
class IdentityInvalidSubject(_BaseEnigmaException): pass
class IdentityInvalidSelfSignature(_BaseEnigmaException): pass
class IdentityInvalidPassphrase(_BaseEnigmaException):
    t = {\
        "too-short": ("passphrase-too-short", 0)
    }
class IdentityNotUsable(_BaseEnigmaException): pass
