"""
Encode/Decode following objects in ASCII mode. Similar to GPG's armored output.

enigma.identity: PublicIdentity, PrivateIdentity
enigma.message: MessagePayload, MessageCryptor

MessagePayload will be represented in different ways depending on the actual
payload's type. For FilePayload it will be encoded, for PlaintextPayload the
plaintext will be displayed. For IdentityTrustPayload a detailed format of
description will be given.
"""
import hashlib
from base64 import b64encode, b64decode
import math

from .identity import PublicIdentity, PrivateIdentity
from .message import MessagePayload, MessageCryptor
from .payload import PlaintextPayload, FilePayload, IdentityTrustPayload

CHECKSUM_ALGONAME = 'SHA256'
CHECKSUM_ALGO = hashlib.sha256

def _b64encode(binary, fixWidth=False):
    ret = b64encode(binary)
    if not fixWidth: return ret
    width = 64 
    imax = int(math.ceil(len(ret) * 1.0 / width))
    ret2 = []
    for i in xrange(0, imax):
        ret2.append(ret[i * width:][:width])
    return "\n".join(ret2)

def _encodeBinary(title, binary):
    global CHECKSUM_ALGONAME, CHECKSUM_ALGO
    title = title.strip().upper()
    head = "---- BEGIN ENIGMA %s ----" % title
    tail = "---- END ENIGMA %s ----" % title
    checksum = CHECKSUM_ALGO(binary).digest()
    ret = '\n'.join([\
        head,
        "CHECKSUM: %s %s" % (CHECKSUM_ALGONAME, _b64encode(checksum)),
        "",
        _b64encode(binary, True),
        tail,
    ])
    return ret

def _decodeBinary(string):
    pass

##############################################################################

def encode(obj):
    if isinstance(obj, PrivateIdentity):
        return _encodeBinary('PRIVATE IDENTITY', str(obj))
    if isinstance(obj, PublicIdentity):
        return _encodeBinary('PUBLIC IDENTITY', str(obj))

    pass

def decode(txt):
    pass
