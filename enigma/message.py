"""
Class Definition of `MessagePayload` and `MessageCryptor`
=========================================================

## MessagePayload

MessagePayload is the class for holding a plaintext-type of information, and,
when necessary, signatures on it.

## MessageCryptor

MessageCryptor class carries an instance of MessagePayload, which is set using
`payload()` method and retrieved after exception-free calling of `decrypt()`
method. Use `encrypt()` method to encrypt the message to a given receiver.

The receiver of encrypted message is by default recorded in the final output.
By calling `encrypt(RECEIVER, True)` the receiver's recorded fingerprint will
be replaced with anonymous identifier `****************`. To decrypt such
messages, the receiving buddy has to pass the second argument as True in
`decrypt()` method call to enable a brute-forced decryption, by trying all
hints carried within a ciphertext. Surely, this is OK if the receiving buddy
owns only a few private identites.
"""

from .serialize.elements import ShortBufferArray, ConstantBuffer, LargeBuffer,\
                                ShortBuffer, FingerprintBuffer,\
                                FingerprintBufferArray
from .serialize.structure import Structure
from .identity import PrivateIdentity, PublicIdentity
from .payload import _BasePayload, parsePayload
from .primitive.random import randomBytes
from .primitive.sym import Encryptor, Decryptor
from .primitive.hash import hash512
from .exceptions import MessageInvalidSigner, MessageInvalidReceiver,\
                        MessageInvalidPayload, MessageInvalidCiphertext

##############################################################################

# -------- Payload of message and its signature, for internal usage. 

# TODO allow or disallow anonymous signature!! FingerprintBufferArray allows
# **************** as exceptional fingerprint for anonymous identity. Whether
# this should appear in MessagePayload, we don't know. And current
# implementation is not adapted to such possibilities due to the use of dict
# type(not allowing multiple anonymous signer).

class MessagePayload:
    """MessagePayload class carries plaintext, its digest, and may also
    the signatures."""

    __structure = Structure(\
        _=ConstantBuffer('p'),
        digest=ShortBuffer(),
        signers=FingerprintBufferArray(),
        signatures=ShortBufferArray(),
        message=LargeBuffer(),
    )

    __signatures = {}
    __message = None

    def __loadMessage(self, s):
        parsed = self.__structure.loads(s)
        if None == parsed: raise MessageInvalidPayload
        # see if signatures can be loaded
        if len(parsed.signers) != len(parsed.signatures):
            raise MessageInvalidPayload
        self.__signatures = {}
        for i in xrange(0, len(parsed.signers)):
            fingerprint = parsed.signers[i].lower()
            signature = parsed.signatures[i]
            self.__signatures[fingerprint] = signature
        # try to load the message and verify intergrity using own digest
        self.__setMessage(parsed.message)
        if self.__messageDigest != parsed.digest:
            raise MessageInvalidPayload

    def __init__(self, s=None):
        if None != s: self.__loadMessage(s)

    def __setMessage(self, message):
        self.__message = str(message)
        self.__messageDigest = hash512(self.__message)

    def message(self, message=None):
        if None != message:
            if not isinstance(message, _BasePayload):
                raise MessageInvalidPayload(\
                    "Requires a detailed payload instance."
                )
            self.__setMessage(str(message))
            return self
        else:
            return parsePayload(self.__message)

    def sign(self, signer):
        if not isinstance(signer, PrivateIdentity):
            # message only allowed to be signed with a private identity
            raise MessageInvalidSigner
        signerPublic = signer.getPublicIdentity()
        fp = signerPublic.getFingerprint()
        signature = signer.sign(self.__messageDigest)
        self.__signatures[fp] = signature

    def listSignatures(self):
        return self.__signatures.keys()

    def verify(self, signer):
        """Support a signer identity, which must be a public identity, and
        verify this message against it using the signature recorded in
        payload."""
        if not isinstance(signer, PublicIdentity):
            raise MessageInvalidSigner
        signerFingerprint = signer.getFingerprint()
        if not self.__signatures.has_key(signerFingerprint):
            raise MessageInvalidSigner
        signature = self.__signatures[signerFingerprint]
        return signer.verify(self.__messageDigest, signature)

    def __str__(self):
        signers = self.listSignatures() 
        signatures = [self.__signatures[i] for i in signers]
        return self.__structure.dumps(\
            _='p',
            digest=self.__messageDigest,
            message=self.__message,
            signers=signers,
            signatures=signatures
        )

    def __repr__(self):
        if self.__message == None: return "<MessagePayload: empty>"
        ret = "<MessagePayload\n"
        signers = self.listSignatures() 
        if len(signers) < 1:        
            ret += "  Signed: No\n"
        else:
            ret += "  Signed by: %s\n" % " ".join(signers)
        ret += "  Message: %d bytes\n" % len(self.__message)
        ret += ">"
        return ret
        

# -------- use MessageCryptor to encrypt a message payload.

class MessageCryptor:

    ANONYMOUS = '****************' # fake anonymous identity fingerprint

    __ciphertext = None
    __encryptKeys = []
    
    __plainKey = None
    __plaintext = None

    __structure = Structure(\
        _=ConstantBuffer('m'),
        receivers=FingerprintBufferArray(),
        decryptors=ShortBufferArray(),
        ciphertext=LargeBuffer()
    )

    def __loadCiphertext(self, s):
        parsed = self.__structure.loads(s)
        if None == parsed: raise MessageInvalidCiphertext
        if len(parsed.receivers) != len(parsed.decryptors):
            raise MessageInvalidCiphertext
        self.__ciphertext = parsed.ciphertext
        self.__encryptKeys = [] 
        for i in xrange(0, len(parsed.receivers)):
            self.__encryptKeys.append(\
                (parsed.receivers[i], parsed.decryptors[i])
            )

    def __init__(self, s=None):
        if s: self.__loadCiphertext(s)

    def payload(self, payload):
        """Set the instance with a new payload and initialize a corresponding
        encryption main key for this payload."""
        if not isinstance(payload, MessagePayload):
            raise MessageInvalidPayload
        self.__plaintext = str(payload)
        self.__plainKey = randomBytes(32)
        return self

    def __tryDecrypt(self, receiver, decryptHint):
        """Try to decrypt this message using a given identity and a
        decryptHint, which should be one entry in the self.__encryptKeys.
        Returns None on failure, otherwise a MessagePayload instance."""
        # ---- use receiver to decrypt the main key
        decryptKey = receiver.decrypt(decryptHint)
        if None == decryptKey: return None
        # ---- use decryptKey to decrypt actual ciphertext
        decryptor = Decryptor(decryptKey)
        plaintext = decryptor.decrypt(self.__ciphertext)
        if None == plaintext: return None
        # ---- if plaintext obtained, record this and return MessagePayload
        #      instance
        self.__plaintext = plaintext
        self.__plainKey = decryptKey
        return MessagePayload(self.__plaintext)

    def decrypt(self, receiver, force=False):
        """Decrypt the message with a given PrivateIdentity. If `force` is NOT
        set True, will just decrypt with recorded decryption hint. Otherwise
        all hints will be tried. On sucessful decryption, will try to construct
        an instance of MessagePayload(this may still cause exception!).
        Otherwise raise an exception of MessageInvalidReceiver."""
        # ---- shortcut, may already decrypted
        if self.__plaintext != None: return MessagePayload(self.__plaintext)
        # ---- check if receiver can be used to decrypt
        if not isinstance(receiver, PrivateIdentity):
            raise MessageInvalidReceiver
        # ---- see how to decrypt, if `force` is set to True, will just try to
        #      decrypt against all decryptor hints. If not, just the hint
        #      matching the given fingerprint will be tried.
        if force:
            for fingerprint, hint in self.__encryptKeys:
                ret = self.__tryDecrypt(receiver, hint)
                if None != ret: return ret
        else:
            receiverPublic = receiver.getPublicIdentity()
            receiverFingerprint = receiverPublic.getFingerprint()
            for fingerprint, hint in self.__encryptKeys:
                if fingerprint != receiverFingerprint: continue
                ret = self.__tryDecrypt(receiver, hint)
                if None != ret: return ret
        # ---- raise exception on failure of decryption
        raise MessageInvalidReceiver
        
    def encrypt(self, receiver, anonymous=False):
        """Encrypt the message to a given PublicIdentity. Use `anonymous` to
        control if the receiver's fingerprint should be included. If this is
        set to True, the receiver has to try against all hints to decrypt."""
        if not isinstance(receiver, PublicIdentity):
            raise MessageInvalidReceiver
        fingerprint = receiver.getFingerprint()
        encryptedKey = receiver.encrypt(self.__plainKey)
        if not anonymous:
            self.__encryptKeys.append((fingerprint, encryptedKey))
        else:
            self.__encryptKeys.append((self.ANONYMOUS, encryptedKey))

    def listReceivers(self):
        """Lists all receivers. Anonymous receiver hints are excluded."""
        receivers = []
        for r, d in self.__encryptKeys:
            if self.ANONYMOUS == r: continue
            receivers.append(r)
        return receivers 

    def __str__(self):
        """Generates the final message. First encrypt the payload using
        self.__plainKey, then export the serialized string."""
        ciphertext = Encryptor(self.__plainKey).encrypt(self.__plaintext)
        receivers, decryptors = [], []
        for r, d in self.__encryptKeys:
            receivers.append(r)
            decryptors.append(d)
        return self.__structure.dumps(\
            _='m',
            ciphertext=ciphertext,
            receivers=receivers,
            decryptors=decryptors
        )

    def __repr__(self):
        if self.__ciphertext == None and self.__plaintext == None:
            return "<MessageCryptor: empty>"
        ret = "<MessageCryptor\n"
        receivers = [r for r,d in self.__encryptKeys if r != ('*' * 16)]
        anonymousCount = len(self.__encryptKeys) - len(receivers)
        if len(receivers) < 1:        
            ret += "  Encrypted: No\n"
        else:
            ret += "  Encrypted to: %s" % " ".join(receivers)
            if anonymousCount > 0:
                ret += " and %d anonymous identities\n" % anonymousCount
            else:
                ret += "\n"
        if self.__plaintext:
            ret += "  Plaintext: %d bytes\n" % len(self.__plaintext)
        else:
            ret += "  Plaintext: Not Available\n"
        ret += ">"
        return ret
