from .en import registerTranslationEN

_TRANSLATIONS = {}
_LANGUAGE = 'en'

class TranslationRegister:

    def __init__(self, languageName):
        self.__languageName = languageName
        if not _TRANSLATIONS.has_key(languageName):
            _TRANSLATIONS[languageName] = {}

    def __enter__(self):
        return self

    def d(self, name, translationFunc):
        _TRANSLATIONS[self.__languageName][name] = translationFunc

    def __exit__(self, a, b, c):
        pass


def configLanguage(name):
    global _LANGUAGE
    LANGUAGE = name

def translate(name, args):
    global _LANGUAGE, _TRANSLATIONS
    try:
        t = _TRANSLATIONS[_LANGUAGE][name]
        return t(*args)
    except Exception,e:
        return "(Translation Error)%s" % name

registerTranslationEN(TranslationRegister('en'))
