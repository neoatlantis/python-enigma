"""Structure definition and determinstic serialization/unserialization"""

from .elements import _BaseElement

class InvalidSerializationStructureDefinition(Exception): pass
class InvalidSerializationStructureValue(ValueError): pass

##############################################################################

class StructureLoadResult: pass

class Structure:

    __def = None
    __keyorder = None

    def __init__(self, **definition):
        for key in definition:
            instance = definition[key]
            if not isinstance(instance, _BaseElement):
                raise InvalidSerializationStructureDefinition
        self.__def = definition
        self.__keyorder = sorted(self.__def.keys())

    def dumps(self, **values):
        ret = []
        for key in values:
            if not self.__def.has_key(key):
                raise InvalidSerializationStructureValue
        for key in self.__keyorder:
            if not values.has_key(key):
                raise InvalidSerializationStructureValue
            ret.append(self.__def[key].dumps(values[key]))
        return ''.join(ret)

    def loads(self, string):
        ret = StructureLoadResult()
        start = string
        for key in self.__keyorder:
            try:
                result, start = self.__def[key].loads(start)
            except Exception,e:
                return None
            setattr(ret, key, result)
        return ret
            
