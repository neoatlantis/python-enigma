"""Defines several `element` classes for serializer. They are both workers and
parameters for defining a structure that's to be serialized."""

import struct
import re

class InvalidSerializationElementDefinition(Exception): pass
class InvalidSerializationType(TypeError): pass
class InvalidSerializationValue(ValueError): pass

##############################################################################

def _filterFingerprint(s):
    # returns a normalized string for valid fingerprint input(in string),
    # otherwise return None
    if s == '****************': return s # for anonymous identity
    valid = re.match('^[2-7a-zA-Z]{16}$', s)
    if None != valid: return s.lower()
    return None

##############################################################################

class _BaseElement: pass

# -------- An assertion alike constant, that requires the buffer must have
#          a same sequence of this value

class ConstantBuffer(_BaseElement):
    
    def __init__(self, value):
        if type(value) != str or len(value) < 1:
            raise InvalidSerializationElementDefinition
        self.__value = value

    def dumps(self, obj):
        return self.__value

    def loads(self, obj):
        if type(obj) != str or not obj.startswith(self.__value):
            raise InvalidSerializationValue
        return (self.__value, obj[len(self.__value):]) # result, next

# -------- A short buffer with no more than 255 bytes. Can be null length.

class ShortBuffer(_BaseElement):
    
    def __init__(self): pass

    def dumps(self, obj):
        if type(obj) != str: raise InvalidSerializationType
        objlen = len(obj)
        if objlen > 256: raise InvalidSerializationValue
        return chr(objlen) + obj

    def loads(self, obj):
        if type(obj) != str or len(obj) < 1: raise InvalidSerializationValue
        objlen = ord(obj[0])
        obj = obj[1:]
        if len(obj) < objlen: raise InvalidSerializationValue
        return (obj[:objlen], obj[objlen:]) # (result, next)

class FingerprintBuffer(ShortBuffer): # special case for fingerprint str only

    def loads(self, obj):
        r, n = ShortBuffer.loads(self, obj)
        r = _filterFingerprint(r)
        if r == None: raise InvalidSerializationValue
        return r, n

# -------- A short buffer with no more than 4Gbytes. Can be null length.

class LargeBuffer(_BaseElement):
    
    def __init__(self): pass

    def dumps(self, obj):
        if type(obj) != str: raise InvalidSerializationType
        objlen = len(obj)
        if objlen > 0xFFFFFFFF: raise InvalidSerializationValue
        return struct.pack('I', objlen) + obj

    def loads(self, obj):
        if type(obj) != str or len(obj) < 4: raise InvalidSerializationValue
        objlen = struct.unpack('I', obj[:4])[0]
        obj = obj[4:]
        if len(obj) < objlen: raise InvalidSerializationValue
        return (obj[:objlen], obj[objlen:]) # (result, next)

# -------- An array of ShortBuffer with < 256 entries

class ShortBufferArray(_BaseElement):
    
    def __init__(self): pass

    def dumps(self, obj):
        if type(obj) != list: raise InvalidSerializationType
        objlen = len(obj)
        if objlen > 256: raise InvalidSerializationValue
        shortBuffer = ShortBuffer()
        buffers = [shortBuffer.dumps(i) for i in obj]
        return chr(objlen) + ''.join(buffers)

    def loads(self, obj):
        if type(obj) != str or len(obj) < 1: raise InvalidSerializationValue
        objlen = ord(obj[0])
        obj = obj[1:]
        ret = []
        shortBuffer = ShortBuffer()
        for i in xrange(0, objlen):
            buf, obj = shortBuffer.loads(obj)
            ret.append(buf)
        return ret, obj

class FingerprintBufferArray(ShortBufferArray): # special case for fingerprints

    def loads(self, obj):
        r, n = ShortBufferArray.loads(self, obj)
        for i in xrange(0, len(r)):
            f = _filterFingerprint(r[i])
            if f == None: raise InvalidSerializationValue
            r[i] = f
        return r, n

# -------- A int64 holder, supports either a 64-bit signed integer, or a None
#          type for nothing. Can be used to hold timestamps(accuracy 1 second).

class NullableInt64(_BaseElement):

    def __init__(self): pass

    def dumps(self, obj):
        if obj != None and type(obj) != int and type(obj) != long:
            raise InvalidSerializationValue
        if obj == None: return '\x00'
        return '\xFF%s' % struct.pack('<q', obj)

    def loads(self, obj):
        objlen = len(obj)
        if type(obj) != str or objlen < 1: raise InvalidSerializationValue
        if obj[0] == '\x00':
            return None, obj[1:]
        elif obj[0] == '\xFF':
            if objlen < 9: raise InvalidSerializationValue
            value = struct.unpack('<q', obj[1:9])[0]
            return value, obj[9:]
        else:
            raise InvalidSerializationValue
