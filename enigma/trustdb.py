"""
Provides a TrustDB instance associated with a given public key.

The TrustDB instance saves and reviews a list of IdentityTrustPayload
signatures on a given public identity.

"""

import hashlib
import time

from .message import MessagePayload
from .payload import IdentityTrustPayload,\
                     InvalidPayload, InvalidPayloadParameter
from .identity import PublicIdentity


class TrustDBOpenedReadOnly(Exception): pass
class TrustDBNotAcceptablePayload(ValueError): pass


class TrustDB:

    def __init__(self, storage, fingerprint):
        """This constructor function is assumed to be called from `Storage`
        class ONLY. Therefore the arguments are trusted and not examined."""
        self.__storage = storage
        self.__db = storage._Storage__db
        self.__ro = storage._Storage__ro
        self.__fingerprint = fingerprint

    def __getTrustDBKey(self, trustPayload):
        return 'trust-%s-%s' % (\
            self.__fingerprint,
            hashlib.sha256(str(trustPayload)).hexdigest()
        )

    def attachPayload(self, msgPayload):
        if self.__ro: raise TrustDBOpenedReadOnly
        if not isinstance(msgPayload, MessagePayload):
            raise TrustDBNotAcceptablePayload
        try:
            trustPayload = msgPayload.message()
            if not isinstance(trustPayload, IdentityTrustPayload):
                raise Exception 
            if trustPayload.destinated != self.__fingerprint:
                raise Exception 
        except:
            raise TrustDBNotAcceptablePayload
        trustHash = self.__getTrustDBKey(trustPayload) 
        self.__db[trustHash] = str(msgPayload)

    def reportTrusts(self):
        """Returns a list of MessagePayload instances that carries
        IdentityTrustPayload instance associated with this identity."""
        collectedTrusts = []
        collectPrefix = 'trust-%s-' % self.__fingerprint
        for each in self.__db:
            if not each.startswith(collectPrefix): continue
            try:
                payload = MessagePayload(self.__db[each])
                trustInfo = payload.message()
                assert isinstance(trustInfo, IdentityTrustPayload)
            except:
                continue
            collectedTrusts.append(payload)
        return collectedTrusts

    def reportValidTrusts(self):
        """Returns verified IdentityTrustPayload(s). Returns a list of tuples,
        in format of (INSTANCE, [VERIFIED SIGNER'S FINGERPRINTS])."""
        # ---- Get trust infos related.
        collectedTrusts = self.reportTrusts()
        # ---- First collect the MessagePayload that contains one trust info.
        #      Then generate the dict that covers all required identites for
        #      verification.
        requiredIdentityFingerprints = {}
        for payload in collectedTrusts:
            signers = payload.listSignatures()
            for each in signers: 
                if not requiredIdentityFingerprints.has_key(each):
                    requiredIdentityFingerprints[each] = ''
        # ---- Load PublicIdentity that will be used.
        for each in requiredIdentityFingerprints:
            try:
                requiredIdentityFingerprints[each] = \
                    self.__storage.publicIdentity(each)
            except: # on not found of this public identity
                del requiredIdentityFingerprints[each]
        # ---- do verifications based on signatures
        signatureValidatedTrusts = []
        for payload in collectedTrusts:
            signers = payload.listSignatures()
            validated = []
            # ---- check signatures of all signers
            for signerFp in signers:
                if not requiredIdentityFingerprints.has_key(signerFp): continue
                try:
                    signer = requiredIdentityFingerprints[signerFp]
                    verifyResult = payload.verify(signer)
                except:
                    verifyResult = False
                if not verifyResult: continue
                validated.append(signerFp)
            # ---- collect verification result
            signatureValidatedTrusts.append((payload.message(), validated))
        # ---- do verifications based on message's validFromUTC and validToUTC
        #      ranges
        nowtime = int(time.time())
        validatedTrusts = []
        for trust, _ in signatureValidatedTrusts:
            if trust.validFromUTC != None and trust.validFromUTC > nowtime:
                continue
            if trust.validToUTC != None and nowtime > trust.validToUTC:
                continue
            validatedTrusts.append((trust, _))
        # ---- return value
        return validatedTrusts
