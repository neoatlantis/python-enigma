"""
Several payloads that can be fit into enigma.message.MessagePayload class.

These payloads are used with special intentions but can always be serialized
and fitted into the MessagePayload class. They are simply different structured
data.

CAUTION: Only use `fill` method to set a value to payload. Do NOT assign values
directly since most `fill` method will do some checking procedures.
"""
import os
from .serialize.structure import Structure
from .serialize.elements import ShortBuffer, LargeBuffer, ConstantBuffer,\
                                ShortBufferArray, NullableInt64,\
                                FingerprintBuffer
from .identity import PublicIdentity


class InvalidPayload(ValueError): pass # on unserialization error
class InvalidPayloadParameter(ValueError): pass # on wrong parameter filled


class _BasePayload:
    def __eq__(self, other):
        return self.__str__() == str(other)



class PlaintextPayload(_BasePayload):
    """Payload holding arbitary plaintext."""

    __structure = Structure(\
        _=ConstantBuffer('1'),
        plaintext=LargeBuffer()
    )

    def __init__(self, s=None):
        if s == None: return
        parsed = self.__structure.loads(s)
        if parsed == None: raise InvalidPayload
        self.plaintext = parsed.plaintext

    def fill(self, plaintext):
        if type(plaintext) != str:
            raise InvalidPayloadParameter
        self.plaintext = plaintext

    def __str__(self):
        return self.__structure.dumps(\
            _='1',
            plaintext=self.plaintext
        )

    def __repr__(self):
        if self.plaintext:
            return "<PlaintextPayload: plaintext=%d bytes>" %\
                len(self.plaintext)
        else:
            return "<PlaintextPayload: empty>"



class FilePayload(_BasePayload):
    """Payload holding a binary file with suggested filename. This class will
    be initialized just by specifying the to-be-included filename. It then
    reads the file and records necessary info."""

    __structure = Structure(\
        _=ConstantBuffer('2'),
        filename=ShortBuffer(),
        data=LargeBuffer()
    )

    def __init__(self, s=None):
        if s == None: return
        parsed = self.__structure.loads(s)
        if parsed == None: raise InvalidPayload
        self.filename = parsed.filename
        self.data = parsed.data

    def fill(self, filename):
        if type(filename) != str or not os.path.isfile(filename):
            raise InvalidPayloadParameter
        self.filename = os.path.basename(filename)
        self.data = open(filename, 'r').read()

    def __str__(self):
        return self.__structure.dumps(\
            _='2',
            filename=self.filename,
            data=self.data
        )


class IdentityTrustPayload(_BasePayload):
    """Payload holding several Key-Value pairs and a specified public identity
    fingerprint. Can be used to express some trust/untrust/authorization onto
    a given public identity.
    
    IMPORTANT: This payload is therefore reasonably to be signed when being
    fitted into MessagePayload class(see `message.py`)."""

    __structure = Structure(\
        _=ConstantBuffer('2'),
        destinated=FingerprintBuffer(),
        keys=ShortBufferArray(),
        values=ShortBufferArray(),
        validFromUTC=NullableInt64(),
        validToUTC=NullableInt64()
    )

    destinated = None

    def __init__(self, s=None):
        if None == s: return
        parsed = self.__structure.loads(s)
        if None == parsed: raise InvalidPayload
        self.destinated = parsed.destinated
        kvlen = len(parsed.keys)
        if kvlen != len(parsed.values): raise InvalidPayload
        self.kv = {}
        for i in xrange(0, kvlen): self.kv[parsed.keys[i]] = parsed.values[i]
        self.validFromUTC = parsed.validFromUTC
        self.validToUTC = parsed.validToUTC

    def fill(self, destinated, kv=None, validFromUTC=None, validToUTC=None):
        try:
            assert type(kv) == dict
            assert type(destinated) == str
            assert None == validFromUTC or\
                type(validFromUTC) in [long, float, int]
            assert None == validToUTC or type(validToUTC) in [long, float, int]
        except:
            raise InvalidPayloadParameter

        self.destinated = destinated
        self.kv = kv
        if None == validFromUTC:
            self.validFromUTC = None
        else:
            self.validFromUTC = int(validFromUTC)
        if None == validToUTC:
            self.validToUTC = None
        else:
            self.validToUTC = int(validToUTC)

    def __str__(self):
        return self.__structure.dumps(\
            _='2',
            destinated=self.destinated,
            keys=self.kv.keys(),
            values=self.kv.values(),
            validFromUTC=self.validFromUTC,
            validToUTC=self.validToUTC
        )

    def __repr__(self):
        if self.destinated != None:
            ret = "<IdentityTrustPayload\n"
            ret += "  destinated=%s\n" % self.destinated
            ret += "  validFromUTC=%s\n" % str(self.validFromUTC)
            ret += "  validToUTC=%s\n" % str(self.validToUTC)
            ret += ">"
            return ret
        else:
            return "<IdentityTrustPayload: empty>"


class PublicIdentityPayload(_BasePayload):
    """Payload holding a PublicIdentity."""

    __structure = Structure(\
        _=ConstantBuffer('4'),
        publicIdentity=ShortBuffer(),
    )

    def __init__(self, s=None):
        if s == None: return
        parsed = self.__structure.loads(s)
        if parsed == None: raise InvalidPayload
        try:
            self.publicIdentity = PublicIdentity(parsed.publicIdentity)
        except:
            raise InvalidPayload

    def fill(self, publicIdentity):
        if not isinstance(publicIdentity, PublicIdentity):
            raise InvalidPayloadParameter
        self.publicIdentity = publicIdentity

    def __str__(self):
        return self.__structure.dumps(\
            _='4',
            publicIdentity=str(self.publicIdentity)
        )



def parsePayload(buf):
    tries = [\
        PlaintextPayload,
        FilePayload,
        IdentityTrustPayload,
        PublicIdentityPayload
    ]
    success = False
    for tryclass in tries:
        try:
            loaded = tryclass(buf)
            success = True
            break
        except InvalidPayload:
            continue
    if not success: raise InvalidPayload
    return loaded
