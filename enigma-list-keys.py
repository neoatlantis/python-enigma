#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

from enigma.storage import Storage, \
                           StorageOpenedReadOnly, \
                           StorageIdentityNotFound

##############################################################################

parser = argparse.ArgumentParser()


parser.parse_args()

##############################################################################

with Storage(readOnly=True) as storage:
    pass
