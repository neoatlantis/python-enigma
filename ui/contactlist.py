# -*- coding: utf-8 -*-

from Tkinter import *

from enigma.identity import PrivateIdentity, PublicIdentity
from ui.util.TextLinkManager import HyperlinkManager

##############################################################################

class IdentityInfoText(Text):

    """The Text specialized for displaying details about a private/public
    identity and its trusts."""

    def __init__(self, root):
        Text.__init__(self, root)
        self.__links = HyperlinkManager(self)
        # ---- init tags
        self.tag_config('normal', font='helvetica 10 normal')
        self.tag_config('bold', font='helvetica 10 bold')
        self.tag_config('publickey', foreground="green")
        self.tag_config('privatekey', foreground="red")
        # ---- lock textbox
        self.configure(state="disabled")

    def insert(self, pos, txt):
        Text.insert(self, pos, txt, ('normal'))

    def insertBold(self, pos, txt):
        Text.insert(self, pos, txt, ('bold'))

    def insertPublicKeyFingerprint(self, pos, txt):
        Text.insert(self, pos, " [", ('bold'))
        Text.insert(self, pos, txt, ('bold', 'publickey'))
        Text.insert(self, pos, "] ", ('bold'))

    def insertPrivateKeyFingerprint(self, pos, txt):
        Text.insert(self, pos, " [", ('bold'))
        Text.insert(self, pos, txt, ('bold', 'privatekey'))
        Text.insert(self, pos, "] ", ('bold'))

    def insertSeparateLine(self, pos):
        Text.insert(self, pos, "\n" + ("=" * 60) + "\n", ('bold'))

    def refresh(self, identity, trustdb):
        self.configure(state="normal")
        # ---- clear text box
        self.delete(0.0, END)
        # ---- identity public/private key
        if isinstance(identity, PublicIdentity):
            self.insertBold(END, "公钥")
            self.insertPublicKeyFingerprint(END, identity.getFingerprint())
        if isinstance(identity, PrivateIdentity):
            self.insertBold(END, "私钥")
            self.insertPrivateKeyFingerprint(END, identity.getFingerprint())
        # ---- subject
        self.insertBold(END, "的标识如下: \n")
        self.insert(END, identity.subject)
        # ---- line
        self.insertSeparateLine(END)
        
        self.configure(state="disabled")


##############################################################################

class IdentityListbox(Listbox):

    __privateList = []
    __publicList = []
    __cursor = None

    __itemIDtoFingerprint = {}
    
    def __init__(self, root):
        Listbox.__init__(self, root)
        self.bind("<<ListboxSelect>>", self.__onSelect)

    def __listItemString(self, identity):
        return "%s [%s]" % (identity["subject"], identity["fingerprint"])

    def refresh(self, privateList, publicList):
        self.__privateList = privateList 
        self.__publicList = publicList 
        self.__itemIDtoFingerprint = {}
        
        self.delete(0, END)

        itemID = 0

        if len(self.__privateList) > 0:
            self.insert(END, "Private Identities")
            self.itemconfig(itemID, bg="black", fg="white")
            for each in self.__privateList: 
                self.insert(END, self.__listItemString(each))
                itemID += 1
                self.__itemIDtoFingerprint[itemID] = each
            
        if len(self.__publicList) > 0:
            self.insert(END, "Public Identities")
            itemID += 1
            self.itemconfig(itemID, bg="black", fg="white")
            for each in self.__publicList: 
                self.insert(END, self.__listItemString(each))
                itemID += 1
                self.__itemIDtoFingerprint[itemID] = each

    def onSelect(self, callback):
        self.__onSelectCallback = callback

    def __onSelect(self, e):
        w = e.widget
        index = int(w.curselection()[0])
        if self.__itemIDtoFingerprint.has_key(index):
            self.__onSelectCallback(self.__itemIDtoFingerprint[index])

##############################################################################

class UIContactList:

    def __init__(self, storage):
        self.__storage = storage
        self.__initWindow()
        self.__initLogic()
        self.refresh()

    def __initWindow(self):
        """Initialize the window."""
        self.__root = Tk()
        # ---- Frame on the left
        self.__fraLeft = Frame(self.__root)
        self.__fraLeft.pack(side=LEFT, fill=BOTH, expand=1)
        ## -------- Label
        self.__label1 = Label(self.__fraLeft, text="Choose one identity")
        self.__label1.pack()
        ## -------- A list containing all private and public identities
        self.__lstIdentity = IdentityListbox(self.__fraLeft)
        self.__lstIdentity.pack(fill=BOTH, expand=1)
        # ---- Frame on the right 
        self.__fraRight = Frame(self.__root)
        self.__fraRight.pack(side=LEFT, fill=BOTH, expand=1)
        ## -------- Textbox for Info Display
        self.__txtInfo = IdentityInfoText(self.__fraRight)
        self.__txtInfo.pack(fill=BOTH, expand=1, side=LEFT)
        ## -------- Scrollbar to Textbox of Info Display
        self.__sclInfo = Scrollbar(self.__fraRight)
        self.__sclInfo.config(command=self.__txtInfo.yview)
        self.__txtInfo.config(yscrollcommand=self.__sclInfo.set)
        self.__sclInfo.pack(side=RIGHT, fill=Y)


    def __refreshIdentityInfo(self, e):
        fingerprint = e["fingerprint"]
        role = e["type"]
        subject = e["subject"]
        with self.__storage as storage:
            if 'public' == role:
                publicIdentity = storage.publicIdentity(fingerprint)
                trustdb = storage.getTrustDB(publicIdentity)
                self.__txtInfo.refresh(publicIdentity, trustdb)
            elif 'private' == role:
                print "<NOT IMPLEMENTED>" # XXX

    def __initLogic(self):
        def refreshIdentityInfo(e): self.__refreshIdentityInfo(e)
        self.__lstIdentity.onSelect(refreshIdentityInfo)

    def refresh(self):
        with self.__storage as storage:
            privateDict = storage.listPrivateIdentities()
            publicDict = storage.listPublicIdentities()
            self.__lstIdentity.refresh(\
                privateDict.values(),
                publicDict.values()
            )

    def show():
        pass
